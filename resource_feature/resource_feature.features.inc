<?php

/**
 * Implementation of hook_node_info().
 */
function resource_feature_node_info() {
  $items = array(
    'resource' => array(
      'name' => t('Resource'),
      'module' => 'features',
      'description' => t('A <em>resource</em> provides the ability to provide a document or a link to the user.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Description'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function resource_feature_views_api() {
  return array(
    'api' => '2',
  );
}
