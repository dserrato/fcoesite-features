<?php

/**
 * Implementation of hook_imagecache_default_presets().
 */
function employee_feature_imagecache_default_presets() {
  $items = array(
    'profile_picture' => array(
      'presetname' => 'profile_picture',
      'actions' => array(
        '0' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_scale',
          'data' => array(
            'width' => '100',
            'height' => '',
            'upscale' => 0,
          ),
        ),
      ),
    ),
  );
  return $items;
}

/**
 * Implementation of hook_node_info().
 */
function employee_feature_node_info() {
  $items = array(
    'employee' => array(
      'name' => t('Employee'),
      'module' => 'features',
      'description' => t('Staff member name, bio and contact information.'),
      'has_title' => '1',
      'title_label' => t('Full Name'),
      'has_body' => '1',
      'body_label' => t('Bio'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function employee_feature_views_api() {
  return array(
    'api' => '2',
  );
}
