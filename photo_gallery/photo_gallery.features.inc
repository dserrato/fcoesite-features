<?php

/**
 * Implementation of hook_imagecache_default_presets().
 */
function photo_gallery_imagecache_default_presets() {
  $items = array(
    'square_thumbnail' => array(
      'presetname' => 'square_thumbnail',
      'actions' => array(
        '0' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_scale',
          'data' => array(
            'width' => '300',
            'height' => '300',
            'upscale' => 0,
          ),
        ),
        '1' => array(
          'weight' => '0',
          'module' => 'imagecache',
          'action' => 'imagecache_crop',
          'data' => array(
            'width' => '175',
            'height' => '175',
            'xoffset' => 'center',
            'yoffset' => 'center',
          ),
        ),
      ),
    ),
  );
  return $items;
}

/**
 * Implementation of hook_node_info().
 */
function photo_gallery_node_info() {
  $items = array(
    'photo_gallery' => array(
      'name' => t('Photo Gallery'),
      'module' => 'features',
      'description' => t('A collection of photos that can be browsed. '),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Description'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}
